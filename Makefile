_build/note.js: _build/note.byte
	js_of_ocaml $<

.PHONY: _build/note.byte
_build/note.byte:
	ocamlbuild -ocamlc 'ocamlfind ocamlc -package js_of_ocaml,js_of_ocaml.syntax,lwt.syntax -syntax camlp4o' -ocamldep 'ocamlfind ocamldep -package js_of_ocaml,js_of_ocaml.syntax,lwt.syntax -syntax camlp4o' -lflags -linkpkg -cflags -annot note.byte

.PHONY: clean
clean:
	ocamlbuild -clean
