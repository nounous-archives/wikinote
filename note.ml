(***************************************************************************)
(*  Copyright © 2010-2013 Stéphane Glondu <steph@glondu.net>               *)
(*                                                                         *)
(*  This program is free software: you can redistribute it and/or modify   *)
(*  it under the terms of the GNU General Public License as published by   *)
(*  the Free Software Foundation, either version 3 of the License, or (at  *)
(*  your option) any later version.                                        *)
(*                                                                         *)
(*  This program is distributed in the hope that it will be useful, but    *)
(*  WITHOUT ANY WARRANTY; without even the implied warranty of             *)
(*  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU      *)
(*  Affero General Public License for more details.                        *)
(*                                                                         *)
(*  You should have received a copy of the GNU General Public License      *)
(*  along with this program.  If not, see <http://www.gnu.org/licenses/>.  *)
(***************************************************************************)

let precision = 2
let epsilon = 10. ** float_of_int (-precision-1)
let document = Dom_html.window##document

let is_class_row class_ x =
  Js.Opt.case (x##getAttribute (Js.string "class"))
    (fun () -> false)
    (fun y -> Js.to_string y = class_)

let extract_text cell =
  let xs = cell##getElementsByTagName (Js.string "p") in
  assert (xs##length = 1);
  let buf = Buffer.create 100 in
  let rec aux x =
    match Js.Opt.to_option x with
      | None -> Buffer.contents buf
      | Some c ->
        if c##nodeType = Dom.TEXT then
          Js.Opt.iter (c##nodeValue) (fun s ->
            Buffer.add_string buf (Js.to_string s)
          );
        aux c##nextSibling
  in
  let inner_cell = (Js.Opt.get (xs##item (0)) (fun _ -> assert false)) in
  inner_cell, String.trim (aux inner_cell##firstChild)

let extract_float s =
  let buf = Buffer.create 100 in
  String.iter
    (function
      | ' ' -> ()
      | c -> Buffer.add_char buf c)
    s;
  let s = Buffer.contents buf in
  if s = "" then 0.
  else (try float_of_string s with _ -> nan)

let format_float x =
  if x > epsilon then Printf.sprintf "+%.*f" precision x
  else if x < (-. epsilon) then Printf.sprintf "%.*f" precision x
  else Printf.sprintf "%.*f" precision 0.

let extract_rev_cells row =
  let xs = row##getElementsByTagName (Js.string "td") in
  let xn = xs##length in
  let rec collect accu i =
    if i < xn then
      let cell = Js.Opt.get (xs##item (i)) (fun () -> assert false) in
      collect (cell::accu) (i+1)
    else accu
  in collect [] 0

let ( ++ ) f g x = g (f x)

let classify_row row =
  if is_class_row "total" row then
    `Total row
  else if is_class_row "ledger-header" row then (
    match extract_rev_cells row with
    | _ :: xs ->
      let headers = List.rev_map (extract_text ++ snd) xs in
      `Headers headers
    | _ -> `None
  ) else if is_class_row "summable" row then (
    match extract_rev_cells row with
    | cell :: xs ->
      let wtf = snd (extract_text cell) in
      let atoms = List.map (extract_text ++ snd ++ extract_float) xs in
      let atoms = Array.of_list (List.rev atoms) in
      let sum = Array.fold_left ( +. ) 0. atoms in
      let check_text =
        if (abs_float sum) < epsilon then " ✔"
        else Printf.sprintf " ✘ (%s)" (format_float sum)
      in
      Dom.appendChild cell
        (document##createTextNode (Js.string check_text));
      `Summable (wtf, atoms)
    | _ -> `None
  ) else `None

let parse_rows (xs : Dom_html.element Dom.nodeList Js.t) =
  let n = xs##length in
  let rec aux (total, accu, headers) i =
    if i < n then
      let x = Js.Opt.get (xs##item (i)) (fun () -> assert false) in
      let arg =
        begin match classify_row x with
          | `Total x -> Some x, accu, headers
          | `Summable xs -> total, xs::accu, headers
          | `Headers xs -> total, accu, Some xs
          | `None -> total, accu, headers
        end
      in
      aux arg (i+1)
    else
      (total, accu, headers)
  in
  aux (None, [], None) 0

let count lines =
  let n = List.fold_left max 0 (List.map Array.length lines) in
  let res = Array.create n 0. in
  List.iter
    (fun line -> Array.iteri (fun i x -> res.(i) <- res.(i) +. x) line)
    lines;
  res

(* https://en.wikipedia.org/wiki/HSL_and_HSV#From_HSV *)
let rgb_of_hsv h s v =
  let c = v *. s in
  let h' = h /. 60. in
  let x = c *. (1. -. abs_float (mod_float h' 2. -. 1.)) in
  let r1, g1, b1 =
    if h' < 1. then (c, x, 0.)
    else if h' < 2. then (x, c, 0.)
    else if h' < 3. then (0., c, x)
    else if h' < 4. then (0., x, c)
    else if h' < 5. then (x, 0., c)
    else if h' < 6. then (c, 0., x)
    else (0., 0., 0.)
  in
  let m = v -. c in
  (r1 +. m, g1 +. m, b1 +. m)

let convert_byte_ratio x =
  min 255 (max 0 (int_of_float (x *. 255.)))

let format_color r =
  let h = 120. *. r in
  let r, g, b = rgb_of_hsv h 0.5 1. in
  Printf.sprintf "#%02X%02X%02X"
    (convert_byte_ratio r)
    (convert_byte_ratio g)
    (convert_byte_ratio b)

let normalize smin smax x =
  if x < (-. epsilon) then 0.5 *. (1. -. x /. smin)
  else if x > epsilon then 0.5 *. (1. +. x /. smax)
  else 0.5

let get_parent_table row =
  let node = (row : Dom_html.element Js.t :> Dom.node Js.t) in
  let rec loop node =
    match String.lowercase (Js.to_string node##nodeName) with
    | "body" -> node
    | "table" -> Js.Opt.case (node##parentNode)
      (fun () -> assert false) (fun x -> x)
    | _ -> Js.Opt.case (node##parentNode) (fun () -> node) loop
  in loop node

let source_link () =
  let p = document##createElement (Js.string "p") in
  let element = document##createElement (Js.string "a") in
  element##setAttribute (
    Js.string "href",
    Js.string "http://git.crans.org/?p=wikinote.git;a=summary"
  );
  let text = document##createTextNode (Js.string "Source code") in
  Dom.appendChild element text;
  Dom.appendChild p element;
  p

let block_header = "-----BEGIN LEDGER-----"
let block_footer = "-----END LEDGER-----"

let format_ledger txs =
  let buf = Buffer.create 256 in
  Buffer.add_string buf block_header;
  Buffer.add_string buf "\n";
  List.iter (fun (wtf, line) ->
    Buffer.add_string buf wtf;
    Buffer.add_string buf " |";
    List.iter (fun (k, v) ->
      Printf.bprintf buf " %s(%s)" k (format_float v)
    ) line;
    Buffer.add_string buf "\n";
  ) txs;
  Buffer.add_string buf block_footer;
  Buffer.add_string buf "\n";
  Buffer.contents buf

let ledger_box txs =
  let pre = document##createElement (Js.string "pre") in
  pre##innerHTML <- Js.string (format_ledger txs);
  pre

let start () =
  let ps = document##getElementsByTagName (Js.string "tr") in
  let total, lines, headers = parse_rows ps in
  let txs =
    match headers with
    | Some hs ->
      let assoc = List.map (fun (wtf, cells) ->
        let full_dict = List.combine hs (Array.to_list cells) in
        wtf, List.filter (fun (k, v) ->
          v < (-. epsilon) || v > epsilon
        ) full_dict
      ) lines in
      Some assoc
    | None -> None
  in
  let nums = List.map snd lines in
  let sums = count nums in
  let normalize = normalize
    (Array.fold_left min max_float sums)
    (Array.fold_left max min_float sums)
  in
  let fill_total x element =
    let z = Js.string (format_float x) in
    let text = document##createTextNode (z) in
    Dom.appendChild element text;
    let c = format_color (normalize x) in
    element##setAttribute (
      Js.string "style",
      Printf.ksprintf Js.string "background-color: %s" c
    )
  in
  (match total with
    | None -> ()
    | Some row ->
      let ys = row##getElementsByTagName (Js.string "td") in
      let set i x = Js.Opt.iter (ys##item (i)) (fill_total x) in
      Array.iteri set sums;
      let table = get_parent_table row in
      Dom.appendChild table (source_link ());
      (match txs with
      | Some txs -> Dom.appendChild table (ledger_box txs)
      | None -> ());
  );
  Lwt.return ()

let () =
  Dom_html.window##onload <- Dom_html.handler (fun _ ->
    ignore (start ());
    Js._false
  )
